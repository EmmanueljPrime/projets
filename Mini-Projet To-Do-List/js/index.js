// Identifiant des postit pour enregistrement et création du DOM
let idPostit = 0
// Variable contenant les informations du formulaire
let formGet = document.querySelector("form")
// Identifiant permettant de mettre à +1 la création de postit
let idForm = 0
let idFormSup = 0

// Séléction des sections HTML correspondant aux trois états des tâches
const waitSection = document.querySelector('#attente')
const inProgressSection = document.querySelector('#encours')
const endSection = document.querySelector('#fini')

/**
 * Permet de créer un élément HTML avec un texte donné
 * @param {string} tagName //ex: 'h2' , 'li'
 * @param {node} content //ex : user.username
 * @returns
 */
function createElementWithText(tagName, content) {
    const element = document.createElement(tagName)
    element.innerText = content
    return element
}
/**
 * Fonction pour créer une div représentant une tâche (post-it) avec des données spécifiques.
 * @param {Object} data - Les données de la tâche.
 * @param {Identifiant} id - L'identifiant de la tâche.
 * @returns {HTMLDivElement} - La div de la tâche créée.
 */
function createTaskDiv(data, id) {
    // Créer une div pour la tâche
    const taskDiv = document.createElement("div")
    taskDiv.classList.add("task", "draggable")
    taskDiv.setAttribute("id", id)
    taskDiv.setAttribute("draggable", "true")
    taskDiv.setAttribute("ondragstart", "drag(event)")

    // Créer les éléments HTML pour le titre et le texte de la tâche
    const titleElement = createElementWithText("h2", data.titleArgument)
    const textElement = createElementWithText("p", data.textArgument)

    // Créer le bouton de suppression
    const suppr = createElementWithText("button", "X")

    // Ajouter les éléments à la div tâche
    taskDiv.appendChild(titleElement)
    taskDiv.appendChild(textElement)
    taskDiv.appendChild(suppr)

    // Ajout d'un gestionnaire d'événement pour la suppression de la tâche
    suppr.addEventListener('click', (event) => {
        localStorage.removeItem("formData" + id)
        event.currentTarget.parentNode.remove()
    })

    // Ajoute la div de la tâche à la section appropriée
    if (data.section == "attente") {
        waitSection.appendChild(taskDiv)
    } else if (data.section == "encours") {
        inProgressSection.appendChild(taskDiv)
    } else if (data.section == "fini") {
        endSection.appendChild(taskDiv)
    }

    // Ajout d'un gestionnaire d'événement pour la mise à jour de la section au lâcher du drag
    taskDiv.addEventListener("dragend", (event) => {

        // Vérifie si l'élément glissé a été déposé dans l'une des sections ("attente," "en cours," ou "fini").
        if (event.target.parentElement.id === waitSection.id) {
            data.section = waitSection.id
        } if (event.target.parentElement.id === inProgressSection.id) {
            data.section = inProgressSection.id
        } if (event.target.parentElement.id === endSection.id) {
            data.section = endSection.id
        }
        // Enregistre les données de section dans le stockage local
        localStorage.setItem("formData" + id, JSON.stringify(data));
    })

    return taskDiv
}

// Fonction pour permettre le glissement d'éléments HTML
function drag(event) {
    event.dataTransfer.setData("text/plain", event.target.id)
}

// Fonction pour autoriser le glissement sur une zône de dépôt
function allowDrop(event) {
    event.preventDefault()
}

/**
 * Fonction pour gérer le dépôt d'éléments glissés dans une zone de dépôt.
 * @param {Event} event - L'événement de glissement (drag-and-drop).
 */
function drop(event) {
    // Récupère les données de l'élément glissé (au format texte)
    const data = event.dataTransfer.getData("text/plain")
    // Récupère l'élément glissable (post-it) à partir de son identifiant
    let draggableElement = document.getElementById(data)
    // Ajoute l'élément glissable à la zone de dépôt actuelle    
    event.currentTarget.appendChild(draggableElement)

}

// Ecouteur d'événement pour soumettre le formulaire
formGet.addEventListener('submit', (event) => {

    // Empêche l'actualisation de la page 
    event.preventDefault()

    // Copie la valeur de "idFormSup" dans "idPostit" pour l'identifiant de la tâche
    idPostit = idFormSup

    // Crée un nouvel objet "FormData" à partir du formulaire HTML
    const formData = new FormData(formGet)
    // Initialise un objet "data" pour stocker les données de la tâche
    let data = {}

    // Parcours les paires de clé-valeur dans l'objet "formData" et les ajoute à l'objet "data"
    for (let pair of formData.entries()) {
        data[pair[0]] = pair[1]
    }

    // Définit la section de la tâche sur "attente"
    data.section = "attente"

    // Enregistre les données de la tâche dans le stockage local avec la clé correspondante
    localStorage.setItem("formData" + idPostit, JSON.stringify(data))

    // Créer la div de la tâche avec les données
    createTaskDiv(data, idPostit)

    // Mise à jour de l'identifiant de post-it pour la prochaine création
    idFormSup++
})
// Ecouteur d'événement pour le chargement du contenu lorsque la page est prête
document.addEventListener("DOMContentLoaded", () => {

    const droppable = document.querySelectorAll(".droppable")
    // Parcours les éléments "droppable" (zones de dépôt) de la page
    for (let element of droppable) {
        // Ajoute un gestionnaire d'événement "dragover" pour autoriser le glissement
        element.addEventListener("dragover", (event) => allowDrop(event));

        // Ajoute un gestionnaire d'événement "drop" pour gérer le glissement d'éléments
        element.addEventListener("drop", (event) => {
            drop(event); // Appelle la fonction "drop" pour gérer le dépôt
            event.preventDefault(); // Empêche le comportement par défaut du navigateur
        });
    }
    // Chargement de tout les post-it depuis le stockage local
    let items = { ...localStorage }

    // Parcours des données de post-it
    for (let storedData of Object.entries(items)) {

        // Parse les données de la tâche depuis le stockage local
        const dataJson = JSON.parse(storedData[1])

        // Extrait l'identifiant unique de la clé de stockage local (en supprimant les 8 premiers caractères)
        const idJson = JSON.parse(storedData[0].slice(8))

        createTaskDiv(dataJson, idJson)

        // Met à jour l'identifiant `idForm` avec la valeur extraite de la clé du stockage local
        idForm = parseInt(storedData[0].slice(8))

        // Boucle pour déterminer la valeur la plus élévé de idFormSup
        if (idJson >= idFormSup) {
            idFormSup = idJson
        }
    }
    // Mise à jour de idFormSup pour la prochiane création
    idFormSup = idFormSup + 1
});




